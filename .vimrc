set hlsearch      " enable highlight on search
syntax enable     " enable syntax highlighting
set number        " enable line numbers
set ignorecase    " ignore case while searching... 
set smartcase     " if we don't put a capital letter
set showmatch     " highlight mathces while searching
set visualbell

set autoindent   
set smartindent
set smarttab
set softtabstop=4

set showmode      " show current mode
set backspace=indent,eol,start " Allow backspace in insert mode
set showcmd       " show incomplete cmds at the bottom
set autoread      " reload files changed outside vim
 
call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'           " Color scheme
Plug 'Valloric/YouCompleteMe'    " Autocompletion plugin

call plug#end()

color gruvbox     " Set color scheme

let g:ycm_global_ycm_extra_conf = "~/.vim/plugged/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py"
